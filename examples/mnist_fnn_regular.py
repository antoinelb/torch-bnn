from typing import Callable, Dict, Tuple

import torch
import torch.nn.functional as F
from torch import Tensor, nn, optim
from torch.optim import Optimizer  # type: ignore
from torch.utils.data import DataLoader
from torchvision import transforms
from torchvision.datasets import MNIST  # pylint: disable=import-error


class Fnn(nn.Module):
    def __init__(self, n_in: int, n_hid: int, n_out: int) -> None:
        super().__init__()
        self.n_in = n_in
        self.n_out = n_out
        self.hidden_layer = nn.Linear(n_in, n_hid)
        self.output_layer = nn.Linear(n_hid, n_out)

    def sample_parameters(self) -> None:
        pass

    def forward(self, inputs: Tensor) -> Tensor:  # type: ignore
        inputs = inputs.reshape(inputs.shape[0], -1)
        hidden = torch.relu(self.hidden_layer(inputs))
        outputs = F.softmax(self.output_layer(hidden), -1)
        return outputs

    def loss(  # pylint: disable=no-self-use
        self, predictions: Tensor, labels: Tensor
    ) -> Tensor:
        return F.nll_loss(torch.log(predictions), labels)

    def predict(self, inputs: Tensor) -> Dict[str, Tensor]:
        predictions = self.forward(inputs)
        return {"predictions": predictions}


def read_data(batch_size: int = 32) -> Tuple[DataLoader, DataLoader]:
    return (
        DataLoader(
            MNIST(
                ".data",
                train=True,
                download=True,
                transform=transforms.ToTensor(),
            ),
            batch_size=batch_size,
            shuffle=True,
        ),
        DataLoader(
            MNIST(
                ".data",
                train=False,
                download=True,
                transform=transforms.ToTensor(),
            ),
            batch_size=batch_size,
            shuffle=False,
        ),
    )


def accuracy(predictions: Dict[str, Tensor], labels: Tensor) -> float:
    return (
        (predictions["predictions"].argmax(-1) == labels).float().mean().item()
    )


def train(
    model: Fnn, data: DataLoader, optimizer: Optimizer, n_epochs: int
) -> Fnn:
    n_batches = len(data)

    for epoch in range(n_epochs):
        total_loss = 0.0
        total_accuracy = 0.0

        for batch, (inputs, labels) in enumerate(data):
            model.sample_parameters()
            optimizer.zero_grad()
            predictions = model(inputs)
            loss_ = model.loss(predictions, labels)
            loss_.backward()
            optimizer.step()
            accuracy_ = accuracy({"predictions": predictions}, labels)
            total_loss += loss_.item()
            total_accuracy += accuracy_
            print(
                f"Epoch {epoch + 1} ({batch + 1}/{n_batches}): "
                f"Loss {loss_.item():.5f}, Accuracy: {accuracy_:.5f}".ljust(
                    80
                ),
                end="\r",
            )

        print(
            f"Epoch {epoch + 1}: "
            f"Loss {total_loss/n_batches:.5f}, "
            f"Accuracy: {total_accuracy/n_batches:.5f}".ljust(80)
        )
    return model


def evaluate(
    model: Fnn, data: DataLoader, metrics: Dict[str, Callable]
) -> Dict[str, float]:
    results = {metric: 0.0 for metric in metrics.keys()}
    for batch, (inputs, outputs) in enumerate(data):
        predictions = model.predict(inputs)
        results = {
            metric: (
                results[metric] * batch * data.batch_size
                + metrics[metric](predictions, outputs) * inputs.shape[0]
            )
            / (batch * data.batch_size + inputs.shape[0])
            for metric in metrics.keys()
        }
    return results


if __name__ == "__main__":
    n_in = 28 * 28
    n_hid = 256
    n_out = 10
    batch_size = 32
    n_epochs = 1

    train_data, test_data = read_data(batch_size)

    model = Fnn(n_in, n_hid, n_out)

    optimizer = optim.SGD(model.parameters(), lr=0.01)

    metrics = {"accuracy": accuracy}

    initial_results = evaluate(model, test_data, metrics)

    model = train(model, train_data, optimizer, n_epochs)

    results = evaluate(model, test_data, metrics)

    print(f"Initial results: {initial_results['accuracy']:.5f}")
    print(f"Final results: {results['accuracy']:.5f}")
