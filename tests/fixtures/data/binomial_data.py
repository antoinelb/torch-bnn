from typing import Tuple

import pytest
import torch
import torch.nn.functional as F
from torch import Tensor

from .utils import add_bias


def fct(inputs: Tensor, n_in: int, n_hid: int) -> Tensor:
    hidden_weights = torch.randn(n_in + 1, n_hid) / 10
    output_weights = torch.randn(n_hid + 1, 1) / 10
    hidden = torch.relu(F.linear(add_bias(inputs), hidden_weights.t()))
    outputs = (
        torch.sigmoid(F.linear(add_bias(hidden), output_weights.t()))
        .round()
        .reshape(-1)
    )
    return outputs


@pytest.fixture
def binomial_data() -> Tuple[Tensor, Tensor]:
    n = 2000
    n_in = 100
    n_hid = 50

    inputs = torch.randn(n, n_in)
    outputs = fct(inputs, n_in, n_hid)
    return inputs, outputs


@pytest.fixture
def other_binomial_data() -> Tuple[Tensor, Tensor]:
    n = 2000
    n_in = 100
    n_hid = 25

    inputs = torch.randn(n, n_in) * 5
    outputs = fct(inputs, n_in, n_hid)
    return inputs, outputs
