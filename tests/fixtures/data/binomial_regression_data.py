from typing import Tuple

import pytest
import torch
import torch.nn.functional as F
from torch import Tensor

from .utils import add_bias


def fct(inputs: Tensor, n_in: int, n_hid: int) -> Tensor:
    hidden_weights = torch.randn(n_hid, n_in + 1) / 10
    output_weights = torch.randn(1, n_hid + 1) / 10

    state = torch.zeros(inputs.shape[0], inputs.shape[2])
    hidden = torch.empty(1, 1, 1)
    for i in range(inputs.shape[1]):
        state = torch.tanh(F.linear(add_bias(inputs[:, i, :]), hidden_weights))
        if i:
            hidden = torch.cat((hidden, torch.unsqueeze(state, 1)), dim=1)
        else:
            hidden = torch.unsqueeze(state, 1)

    outputs = (
        torch.sigmoid(F.linear(add_bias(hidden), output_weights))
        .round()
        .squeeze(-1)
    )
    return outputs


@pytest.fixture
def binomial_regression_data() -> Tuple[Tensor, Tensor]:
    n = 2000
    n_in = 100
    n_hid = 50
    n_sequence = 10

    inputs = torch.randn(n, n_sequence, n_in)
    outputs = fct(inputs, n_in, n_hid)
    return inputs, outputs


@pytest.fixture
def other_binomial_regression_data() -> Tuple[Tensor, Tensor]:
    n = 2000
    n_in = 100
    n_hid = 25
    n_sequence = 10

    inputs = torch.randn(n, n_sequence, n_in) * 5
    outputs = fct(inputs, n_in, n_hid)
    return inputs, outputs
