import torch
from torch import Tensor


def add_bias(inputs: Tensor, dim: int = -1) -> Tensor:
    return torch.cat((torch.ones(*inputs.shape[:-1], 1), inputs), dim=dim)
