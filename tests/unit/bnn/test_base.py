import pytest
from torch import Tensor

from torch_bnn.bnn.base import BayesianBaseLayer


def test_methods_raise_not_implemented():
    class BayesianLayer(BayesianBaseLayer):  # pylint: disable=abstract-method
        def __init__(self) -> None:  # pylint: disable=useless-super-delegation
            super().__init__()

        @property
        def weights(self) -> Tensor:
            pass

        @property
        def kl_loss(self) -> Tensor:
            pass

    layer = BayesianLayer()

    with pytest.raises(NotImplementedError):
        layer.sample_parameters()

    with pytest.raises(NotImplementedError):
        layer.set_to_best_prediction()


def test_properties_raise_not_implemented():
    class BayesianLayer(BayesianBaseLayer):  # pylint: disable=abstract-method
        def __init__(self) -> None:  # pylint: disable=useless-super-delegation
            super().__init__()

    layer = BayesianLayer()

    with pytest.raises(NotImplementedError):
        layer.weights  # pylint: disable=pointless-statement

    with pytest.raises(NotImplementedError):
        layer.kl_loss  # pylint: disable=pointless-statement
