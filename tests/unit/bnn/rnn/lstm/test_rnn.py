from torch_bnn.bnn import BayesianLstm
from torch_bnn.bnn.rnn.lstm import BayesianLstmCell


def test_has_correct_cell():
    rnn = BayesianLstm(10, 10, True)
    assert isinstance(rnn.cell, BayesianLstmCell)
