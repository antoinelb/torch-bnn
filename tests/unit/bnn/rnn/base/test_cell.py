from typing import List, Tuple

import pytest
import torch
from torch import Tensor, jit

from torch_bnn.bnn.rnn.base import BayesianBaseRnnCell


class BayesianRnnCell(BayesianBaseRnnCell):
    def __init__(self, n_in: int, n_out: int, bias: bool):
        super().__init__(n_in, n_out, bias)

    def sample_parameters(self) -> None:
        pass

    def set_to_best_prediction(self) -> None:
        pass

    def init_state(self, batch_size: int) -> List[Tensor]:
        pass

    @jit.script_method
    def forward(
        self, inputs: Tensor, state: List[Tensor]
    ) -> Tuple[Tensor, List[Tensor]]:
        return torch.zeros(1), []

    @property
    def weights(self) -> Tensor:
        pass

    @property
    def kl_loss(self) -> Tensor:
        pass


def test_sets_properties():
    n_in = 5
    n_out = 10
    bias = True
    cell = BayesianRnnCell(n_in, n_out, bias)
    assert cell.n_in == n_in
    assert cell.n_out == n_out
    assert cell.bias == bias


def test_methods_raise_not_implemented():
    class BayesianRnnCell(
        BayesianBaseRnnCell
    ):  # pylint: disable=abstract-method
        def __init__(self, n_in: int, n_out: int, bias: bool):
            super().__init__(n_in, n_out, bias)

        @property
        def weights(self) -> Tensor:
            pass

        @property
        def kl_loss(self) -> Tensor:
            pass

    cell = BayesianRnnCell(5, 10, True)

    with pytest.raises(NotImplementedError):
        cell.sample_parameters()

    with pytest.raises(NotImplementedError):
        cell.set_to_best_prediction()

    with pytest.raises(NotImplementedError):
        cell.init_state(2)

    with pytest.raises(jit.Error):
        cell.forward(torch.zeros(1), [])

    with pytest.raises(jit.Error):
        cell(torch.zeros(1), [])


def test_properties_raise_not_implemented():
    class BayesianRnnCell(
        BayesianBaseRnnCell
    ):  # pylint: disable=abstract-method
        def __init__(self, n_in: int, n_out: int, bias: bool):
            super().__init__(n_in, n_out, bias)

    with pytest.raises(NotImplementedError):
        BayesianRnnCell(5, 10, True)

    class BayesianRnnCell(
        BayesianBaseRnnCell
    ):  # pylint: disable=abstract-method,function-redefined
        def __init__(self, n_in: int, n_out: int, bias: bool):
            super().__init__(n_in, n_out, bias)

        @property
        def kl_loss(self) -> Tensor:
            pass

    with pytest.raises(NotImplementedError):
        BayesianRnnCell(5, 10, True)

    class BayesianRnnCell(
        BayesianBaseRnnCell
    ):  # pylint: disable=abstract-method,function-redefined
        def __init__(self, n_in: int, n_out: int, bias: bool):
            super().__init__(n_in, n_out, bias)

        @property
        def weights(self) -> Tensor:
            pass

    with pytest.raises(NotImplementedError):
        BayesianRnnCell(5, 10, True)
