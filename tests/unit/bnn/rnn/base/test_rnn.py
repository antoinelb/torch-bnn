from typing import List, Tuple
from unittest.mock import PropertyMock

import torch
from torch import Tensor, jit

from torch_bnn.bnn.rnn.base import BayesianBaseRnn, BayesianBaseRnnCell


class BayesianRnn(BayesianBaseRnn):
    def __init__(self, n_in: int, n_out: int, cell, bias: bool):
        super().__init__(n_in, n_out, cell, bias)


def test_sets_properties(mocker):
    n_in = 5
    n_out = 10
    bias = True

    mocker.patch.object(BayesianBaseRnnCell, "weights")
    mocker.patch.object(BayesianBaseRnnCell, "kl_loss")

    rnn = BayesianRnn(n_in, n_out, BayesianBaseRnnCell, bias)
    assert rnn.n_in == n_in
    assert rnn.n_out == n_out
    assert rnn.bias == bias
    assert isinstance(rnn.cell, BayesianBaseRnnCell)


def test_method_calls(mocker):
    class BayesianRnn(BayesianBaseRnn):
        def __init__(self, n_in: int, n_out: int, cell, bias: bool):
            super().__init__(n_in, n_out, cell, bias)

    n_in = 5
    n_out = 10
    bias = True

    sample_parameters = mocker.patch.object(
        BayesianBaseRnnCell, "sample_parameters"
    )
    set_to_best_prediction = mocker.patch.object(
        BayesianBaseRnnCell, "set_to_best_prediction"
    )
    init_state = mocker.patch.object(BayesianBaseRnnCell, "init_state")
    weights = mocker.patch.object(
        BayesianBaseRnnCell, "weights", new_callable=PropertyMock
    )
    kl_loss = mocker.patch.object(
        BayesianBaseRnnCell, "kl_loss", new_callable=PropertyMock
    )

    rnn = BayesianRnn(n_in, n_out, BayesianBaseRnnCell, bias)

    rnn.sample_parameters()
    sample_parameters.assert_called_once_with()

    rnn.set_to_best_prediction()
    set_to_best_prediction.assert_called_once_with()

    rnn.init_state(32)
    init_state.assert_called_once_with(32)

    rnn.weights  # pylint: disable=pointless-statement
    weights.assert_called()

    rnn.kl_loss  # pylint: disable=pointless-statement
    kl_loss.assert_called()


def test_forward():
    class BayesianRnn(BayesianBaseRnn):
        def __init__(self, n_in: int, n_out: int, cell, bias: bool):
            super().__init__(n_in, n_out, cell, bias)

    class BayesianRnnCell(
        BayesianBaseRnnCell
    ):  # pylint: disable=abstract-method
        def __init__(self, n_in: int, n_out: int, bias: bool) -> None:
            super().__init__(n_in, n_in, bias)

        def init_state(self, batch_size: int) -> List[Tensor]:
            return [torch.zeros(batch_size, self.n_out)]

        @jit.script_method
        def forward(
            self, inputs: Tensor, state: List[Tensor]
        ) -> Tuple[Tensor, List[Tensor]]:
            state_ = inputs + state[0]
            return state_, [state_]

        @property
        def weights(self) -> Tensor:
            pass

        @property
        def kl_loss(self) -> Tensor:
            pass

    n_in = 5
    bias = True
    batch_size = 10
    sequence_size = 3

    rnn = BayesianRnn(n_in, n_in, BayesianRnnCell, bias)

    inputs = torch.ones(batch_size, sequence_size, n_in)
    correct = torch.cat(
        [
            (i + 1) * torch.ones(batch_size, 1, n_in)
            for i in range(sequence_size)
        ],
        dim=1,
    )

    outputs, _ = rnn.forward(inputs)
    assert (outputs == correct).all()

    outputs, _ = rnn(inputs)
    assert (outputs == correct).all()
