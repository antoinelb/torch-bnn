from torch_bnn.bnn import BayesianRnn
from torch_bnn.bnn.rnn.basic import BayesianBasicRnnCell


def test_has_correct_cell():
    rnn = BayesianRnn(10, 10, True)
    assert isinstance(rnn.cell, BayesianBasicRnnCell)
