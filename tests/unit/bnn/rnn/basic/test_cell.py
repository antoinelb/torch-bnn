import torch

from torch_bnn.bnn.rnn.basic import BayesianBasicRnnCell


def test_init():
    n_in = 5
    n_out = 10

    cell = BayesianBasicRnnCell(n_in, n_out, bias=True)
    assert cell.weights_mu.shape == (n_out, n_in + n_out + 1)
    assert cell.weights_rho.shape == (n_out, n_in + n_out + 1)
    assert (cell.epsilon == torch.zeros(n_out, n_in + n_out + 1)).all()

    cell = BayesianBasicRnnCell(n_in, n_out, bias=False)
    assert cell.weights_mu.shape == (n_out, n_in + n_out)
    assert cell.weights_rho.shape == (n_out, n_in + n_out)
    assert (cell.epsilon == torch.zeros(n_out, n_in + n_out)).all()


def test_weight():
    n_in = 5
    n_out = 10
    bias = True
    cell = BayesianBasicRnnCell(n_in, n_out, bias)

    assert cell._weights().shape == (  # pylint: disable=protected-access
        n_out,
        n_in + n_out + bias,
    )
    assert cell.weights.shape == (n_out, n_in + n_out + bias)


def test_sample_parameters():
    n_in = 5
    n_out = 10
    bias = True
    cell = BayesianBasicRnnCell(n_in, n_out, bias)

    initial_weights = cell.weights
    cell.sample_parameters()
    assert (cell.weights != initial_weights).any()


def test_set_to_best_prediction():
    n_in = 5
    n_out = 10
    bias = True
    cell = BayesianBasicRnnCell(n_in, n_out, bias)

    cell.set_to_best_prediction()
    assert (cell.weights == cell.weights_mu).all()


def test_init_state():
    n_in = 5
    n_out = 10
    bias = True
    cell = BayesianBasicRnnCell(n_in, n_out, bias)

    batch_size = 3
    state = cell.init_state(batch_size)

    assert len(state) == 1
    assert state[0].shape == (batch_size, n_out)


def test_forward():
    n_in = 5
    n_out = 10
    bias = True
    cell = BayesianBasicRnnCell(n_in, n_out, bias)

    batch_size = 3
    state = cell.init_state(batch_size)

    inputs = torch.randn(batch_size, n_in)

    outputs, state = cell.forward(inputs, state)
    assert outputs.shape == (batch_size, n_out)
    assert len(state) == 1
    assert state[0].shape == (batch_size, n_out)

    outputs, state = cell(inputs, state)
    assert outputs.shape == (batch_size, n_out)
    assert len(state) == 1
    assert state[0].shape == (batch_size, n_out)


def test_kl_loss():
    n_in = 5
    n_out = 10
    bias = True
    cell = BayesianBasicRnnCell(n_in, n_out, bias)

    cell.sample_parameters()
    true_kl_loss = cell.kl_loss.item()

    torch.nn.init.constant_(cell.weights_mu, 0)
    torch.nn.init.constant_(cell.weights_rho, 0.54)
    standard_kl_loss = cell.kl_loss.item()

    assert true_kl_loss > 0
    assert true_kl_loss > standard_kl_loss
