import pytest
import torch

from torch_bnn import bnn


@pytest.fixture
def layer() -> bnn.BayesianLinear:
    n_in = 10
    n_out = 5
    bias = True
    return bnn.BayesianLinear(n_in, n_out, bias)


def test_sample_parameters(layer):
    initial_kl_loss = layer.kl_loss
    layer.sample_parameters()
    assert initial_kl_loss != layer.kl_loss


def test_set_to_best_prediction(layer):
    layer.set_to_best_prediction()
    assert (layer.weights == layer.weights_mu).all()


def test_forward(layer):
    batch_size = 10
    inputs = torch.randn(batch_size, layer.n_in)
    assert layer.forward(inputs).shape == (batch_size, layer.n_out)
    assert layer(inputs).shape == (batch_size, layer.n_out)


def test_weights():
    n_in = 10
    n_out = 5
    bias = True
    layer = bnn.BayesianLinear(n_in, n_out, bias)

    assert layer.weights.shape == (n_out, n_in + 1)

    n_in = 10
    n_out = 5
    bias = False
    layer = bnn.BayesianLinear(n_in, n_out, bias)

    assert layer.weights.shape == (n_out, n_in)


def test_kl_loss():
    n_in = 10
    n_out = 5
    bias = True
    layer = bnn.BayesianLinear(n_in, n_out, bias)

    layer.sample_parameters()
    true_kl_loss = layer.kl_loss.item()

    torch.nn.init.constant_(layer.weights_mu, 0)
    torch.nn.init.constant_(layer.weights_rho, 0.54)
    standard_kl_loss = layer.kl_loss.item()

    assert true_kl_loss > 0
    assert true_kl_loss > standard_kl_loss
