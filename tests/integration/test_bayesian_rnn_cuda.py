from typing import Callable, Dict, Tuple

import pytest
import torch
import torch.nn.functional as F
from torch import Tensor, nn, optim
from torch.optim import Optimizer  # type: ignore
from torch.utils.data import DataLoader, TensorDataset

from torch_bnn import bnn

from .metrics import accuracy, mean_std


class BayesianRnn(nn.Module):
    def __init__(self, n_in: int, n_hid: int, n_samples: int) -> None:
        super().__init__()
        self._n_in = n_in
        self._n_hid = n_hid
        self._n_samples = n_samples

        self._hidden_layer = bnn.BayesianRnn(n_in, n_hid)
        self._output_layer = bnn.BayesianLinear(n_hid, 1)

        self.loss_criterion = F.binary_cross_entropy

    def sample_parameters(self) -> None:
        self._hidden_layer.sample_parameters()
        self._output_layer.sample_parameters()

    def forward(self, inputs: Tensor) -> Tensor:  # type: ignore
        hidden, _ = self._hidden_layer(inputs)
        outputs = torch.sigmoid(self._output_layer(hidden)).squeeze(-1)
        return outputs

    def predict(self, inputs: Tensor) -> Dict[str, Tensor]:
        predictions_ = []
        for _ in range(self._n_samples):
            self.sample_parameters()
            predictions_.append(self.forward(inputs))
        predictions = torch.cat(
            [torch.unsqueeze(prediction, 0) for prediction in predictions_],
            dim=0,
        )
        mean = predictions.mean(0)
        std = predictions.std(0)
        credibility_interval = (mean - 1.96 * std, mean + 1, 96 * std)
        return {
            "predictions": mean,
            "std": std,
            "credibility_interval_lower": credibility_interval[0],
            "credibility_interval_upper": credibility_interval[1],
        }

    def loss(
        self,
        predictions: Tensor,
        outputs: Tensor,
        kl_loss_adjustment: float = 1,
    ) -> Tensor:
        likelihood = self.loss_criterion(predictions, outputs)
        kl_loss = self._hidden_layer.kl_loss + self._output_layer.kl_loss
        return likelihood + kl_loss_adjustment * kl_loss


def train(
    model: BayesianRnn, data: DataLoader, optimizer: Optimizer
) -> BayesianRnn:
    for inputs, outputs in data:
        inputs = inputs.cuda()
        outputs = outputs.cuda()
        model.sample_parameters()
        optimizer.zero_grad()
        predictions = model(inputs)
        loss_ = model.loss(predictions, outputs)
        loss_.backward()
        optimizer.step()
    return model


def evaluate(
    model: BayesianRnn, data: DataLoader, metrics: Dict[str, Callable]
) -> Dict[str, float]:
    results = {**{"loss": 0.0}, **{metric: 0.0 for metric in metrics.keys()}}
    for batch, (inputs, outputs) in enumerate(data):
        inputs = inputs.cuda()
        outputs = outputs.cuda()
        predictions = model.predict(inputs)
        results = {
            metric: (
                results[metric] * batch * data.batch_size
                + metrics[metric](predictions, outputs) * inputs.shape[0]
            )
            / (batch * data.batch_size + inputs.shape[0])
            for metric in metrics.keys()
        }
    return results


@pytest.mark.skipif(
    not torch.cuda.is_available(), reason="Requires cuda to run"
)
def test_rnn(
    binomial_regression_data: Tuple[Tensor, Tensor],
    other_binomial_regression_data: Tuple[Tensor, Tensor],
) -> None:
    n_in = binomial_regression_data[0][0].shape[-1]
    n_hid = 50
    n_samples = 5
    batch_size = 32

    model = BayesianRnn(n_in, n_hid, n_samples)
    model.cuda()
    dataset = DataLoader(TensorDataset(*binomial_regression_data), batch_size)
    other_dataset = DataLoader(
        TensorDataset(*other_binomial_regression_data), batch_size
    )

    optimizer = optim.SGD(model.parameters(), lr=0.01)

    metrics = {"accuracy": accuracy, "mean_std": mean_std}

    initial_results = evaluate(model, dataset, metrics)

    model = train(model, dataset, optimizer)

    results = evaluate(model, dataset, metrics)
    assert results["accuracy"] > initial_results["accuracy"]  # nosec

    other_results = evaluate(model, other_dataset, metrics)
    assert other_results["mean_std"] > results["mean_std"]  # nosec
