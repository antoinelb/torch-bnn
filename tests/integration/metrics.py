from typing import Dict

from torch import Tensor


def accuracy(predictions: Dict[str, Tensor], labels: Tensor) -> float:
    return (predictions["predictions"].round() == labels).float().mean().item()


def rmse(predictions: Dict[str, Tensor], outputs: Tensor) -> float:
    return (predictions["predictions"] - outputs).pow(2).mean().sqrt().item()


def mean_std(predictions: Dict[str, Tensor], _: Tensor) -> float:
    return predictions["std"].mean().item()
