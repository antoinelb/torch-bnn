from typing import Callable, Dict, Tuple

import torch
import torch.nn.functional as F
from torch import Tensor, nn, optim
from torch.optim import Optimizer  # type: ignore
from torch.utils.data import DataLoader, TensorDataset

from .metrics import accuracy


class Rnn(nn.Module):
    def __init__(self, n_in: int, n_hid: int) -> None:
        super().__init__()
        self._n_in = n_in
        self._n_hid = n_hid

        self._hidden_layer = nn.RNN(n_in, n_hid, batch_first=True)
        self._output_layer = nn.Linear(n_hid, 1)

        self.loss_criterion = F.binary_cross_entropy

    def sample_parameters(self) -> None:
        pass

    def init_state(self, batch_size: int) -> Tensor:
        return torch.zeros(1, batch_size, self._n_hid)

    def forward(self, inputs: Tensor) -> Tensor:  # type: ignore
        state = self.init_state(inputs.shape[0])
        hidden, _ = self._hidden_layer(inputs, state)
        outputs = torch.sigmoid(self._output_layer(hidden)).squeeze(-1)
        return outputs

    def predict(self, inputs: Tensor) -> Dict[str, Tensor]:
        predictions = self.forward(inputs)
        return {"predictions": predictions}

    def loss(
        self,
        predictions: Tensor,
        outputs: Tensor,
        kl_loss_adjustment: float = 1,
    ) -> Tensor:
        likelihood = self.loss_criterion(predictions, outputs)
        kl_loss = torch.tensor(0)  # pylint: disable=not-callable
        return likelihood + kl_loss_adjustment * kl_loss


def train(model: Rnn, data: DataLoader, optimizer: Optimizer) -> Rnn:
    for inputs, outputs in data:
        model.sample_parameters()
        optimizer.zero_grad()
        predictions = model(inputs)
        loss_ = model.loss(predictions, outputs)
        loss_.backward()
        optimizer.step()
    return model


def evaluate(
    model: Rnn, data: DataLoader, metrics: Dict[str, Callable]
) -> Dict[str, float]:
    results = {**{"loss": 0.0}, **{metric: 0.0 for metric in metrics.keys()}}
    for batch, (inputs, outputs) in enumerate(data):
        predictions = model.predict(inputs)
        results = {
            metric: (
                results[metric] * batch * data.batch_size
                + metrics[metric](predictions, outputs) * inputs.shape[0]
            )
            / (batch * data.batch_size + inputs.shape[0])
            for metric in metrics.keys()
        }
    return results


def test_fnn(binomial_regression_data: Tuple[Tensor, Tensor]) -> None:
    n_in = binomial_regression_data[0][0].shape[-1]
    n_hid = 50
    batch_size = 32

    model = Rnn(n_in, n_hid)
    dataset = DataLoader(TensorDataset(*binomial_regression_data), batch_size)

    optimizer = optim.SGD(model.parameters(), lr=0.01)

    metrics = {"accuracy": accuracy}

    initial_results = evaluate(model, dataset, metrics)

    model = train(model, dataset, optimizer)

    results = evaluate(model, dataset, metrics)
    assert results["accuracy"] > initial_results["accuracy"]  # nosec
