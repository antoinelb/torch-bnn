from typing import Callable, Dict, Tuple

import torch
import torch.nn.functional as F
from torch import Tensor, nn, optim
from torch.optim import Optimizer  # type: ignore
from torch.utils.data import DataLoader, TensorDataset

from torch_bnn import bnn

from .metrics import accuracy, mean_std


class BayesianFnn(nn.Module):
    def __init__(self, n_in: int, n_hid: int, n_samples: int) -> None:
        super().__init__()
        self._n_in = n_in
        self._n_hid = n_hid
        self._n_samples = n_samples

        self._hidden_layer = bnn.BayesianLinear(n_in, n_hid)
        self._output_layer = bnn.BayesianLinear(n_hid, 1)

        self.loss_criterion = F.binary_cross_entropy

    def sample_parameters(self) -> None:
        self._hidden_layer.sample_parameters()
        self._output_layer.sample_parameters()

    def forward(self, inputs: Tensor) -> Tensor:  # type: ignore
        hidden = torch.relu(self._hidden_layer(inputs))
        outputs = torch.sigmoid(self._output_layer(hidden)).reshape(-1)
        return outputs

    def predict(self, inputs: Tensor) -> Dict[str, Tensor]:
        predictions_ = []
        for _ in range(self._n_samples):
            self.sample_parameters()
            predictions_.append(self.forward(inputs))
        predictions = torch.cat(
            [torch.unsqueeze(prediction, 0) for prediction in predictions_],
            dim=0,
        )
        mean = predictions.mean(0)
        std = predictions.std(0)
        credibility_interval = (mean - 1.96 * std, mean + 1, 96 * std)
        return {
            "predictions": mean,
            "std": std,
            "credibility_interval_lower": credibility_interval[0],
            "credibility_interval_upper": credibility_interval[1],
        }

    def loss(
        self,
        predictions: Tensor,
        outputs: Tensor,
        kl_loss_adjustment: float = 1,
    ) -> Tensor:
        likelihood = self.loss_criterion(predictions, outputs)
        kl_loss = self._hidden_layer.kl_loss + self._output_layer.kl_loss
        return likelihood + kl_loss_adjustment * kl_loss


def train(
    model: BayesianFnn, data: DataLoader, optimizer: Optimizer
) -> BayesianFnn:
    kl_loss_adjustment = 1 / len(data)
    for inputs, outputs in data:
        model.sample_parameters()
        optimizer.zero_grad()
        predictions = model(inputs)
        loss_ = model.loss(
            predictions, outputs, kl_loss_adjustment=kl_loss_adjustment
        )
        loss_.backward()
        optimizer.step()
    return model


def evaluate(
    model: BayesianFnn, data: DataLoader, metrics: Dict[str, Callable]
) -> Dict[str, float]:
    results = {**{"loss": 0.0}, **{metric: 0.0 for metric in metrics.keys()}}
    for batch, (inputs, outputs) in enumerate(data):
        predictions = model.predict(inputs)
        results = {
            metric: (
                results[metric] * batch * data.batch_size
                + metrics[metric](predictions, outputs) * inputs.shape[0]
            )
            / (batch * data.batch_size + inputs.shape[0])
            for metric in metrics.keys()
        }
    return results


def test_fnn(
    binomial_data: Tuple[Tensor, Tensor],
    other_binomial_data: Tuple[Tensor, Tensor],
) -> None:
    n_in = binomial_data[0][0].shape[-1]
    n_hid = 100
    n_samples = 5
    batch_size = 32

    model = BayesianFnn(n_in, n_hid, n_samples)
    dataset = DataLoader(TensorDataset(*binomial_data), batch_size)
    other_dataset = DataLoader(TensorDataset(*other_binomial_data), batch_size)

    optimizer = optim.SGD(model.parameters(), lr=0.01)

    metrics = {"accuracy": accuracy, "mean_std": mean_std}

    initial_results = evaluate(model, dataset, metrics)

    model = train(model, dataset, optimizer)

    results = evaluate(model, dataset, metrics)
    assert results["accuracy"] > initial_results["accuracy"]  # nosec

    other_results = evaluate(model, other_dataset, metrics)
    assert other_results["mean_std"] > results["mean_std"]  # nosec
