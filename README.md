[![pipeline status](https://gitlab.com/antoinelb/torch-bnn/badges/master/pipeline.svg)](https://gitlab.com/antoinelb/torch-bnn/-/commits/master)
[![coverage report](https://gitlab.com/antoinelb/torch-bnn/badges/master/coverage.svg)](https://gitlab.com/antoinelb/torch-bnn/-/commits/master)
[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)
[![security: bandit](https://img.shields.io/badge/security-bandit-green.svg)](https://github.com/PyCQA/bandit)

# Pytorch Bayesian Neural Networks

Implements a series of bayesian layers for use in bayesian neural networks.
